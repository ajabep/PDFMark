PDFMark
=======

PDFMark is a tool to mark a PDF file, and facilitate that to retrive it on Internet.


What it does exactly
--------------------

It write, at the bottom left of every pages, the UID in black. To hide the UID, it cover the UID with a white rectangle.

It write the UID in the content to index the UID un search engines. It write it in black to be print and indexed when the user use some converter of the PDF in another format, as in HTML.

It does not use metadatas, because metadatas can easily be cleared[1], metadatas are not indexed by search engine and I usually clear my metadatas.

[1] : cf. Metadata Anonymisation Toolkit


License
-------

The code is under GNU GPL v3. To learn more about it, read [LICENSE](LICENSE)
