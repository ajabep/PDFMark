#!/usr/bin/env python3
#
# Requirements:
#   * PyPDF2
#   * reportlab


from PyPDF2 import PdfFileReader
from PyPDF2 import PdfFileWriter
from reportlab.pdfgen import canvas
import reportlab.lib.pagesizes

import sys


if len(sys.argv) != 4:
    print('Usage %s <pdf to transform> <UID> <output file>' % sys.argv[0])
    sys.exit()


# Use reportlab to create a PDF that will be used as a watermark on another PDF.
c = canvas.Canvas("water", pagesize=reportlab.lib.pagesizes.A4)
c.setFont("Courier", 10)
c.setFillGray(0, .9)
c.saveState()

c.setFillColorRGB(0, 0, 0)
c.drawString(0, 0, sys.argv[2])

c.setFillColorRGB(1, 1, 1)
c.rect(0, 0, 6.2 * len(sys.argv[2]), 10, stroke=0, fill=1)
c.rect(0, 0, 6.2 * len(sys.argv[2]), 10, stroke=0, fill=1)

c.restoreState()
c.save()


# Read in the PDF that will have the PDF applied to it.
output = PdfFileWriter()
input1 = PdfFileReader(open(sys.argv[1], "rb"))
watermark = PdfFileReader(open("water", "rb"))

for page in input1.pages:
    page.mergePage(watermark.getPage(0))
    output.addPage(page)

outputStream = open(sys.argv[3], "wb")
output.write(outputStream)
outputStream.close()
